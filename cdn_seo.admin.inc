<?php

/**
 * @file
 * Admin file.
 */

/**
 * Page generation function for admin/settings/cdn/seo.
 */
function cdn_seo_admin_page() {
  $output = '';
  return $output . drupal_get_form('cdn_seo_admin_settings_form');
}

/**
 * Form builder; Configure cdn seo settings.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function cdn_seo_admin_settings_form() {

  $blacklist = variable_get('cdn_seo_blacklist', cdn_get_domains());
  $form['cdn_seo_blacklist'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Blacklist'),
    '#default_value' => implode("\n", $blacklist),
    '#description'   => t('If you try to access domains in this list, odds are it will return a 404. The exception being if the request comes into the files directory and there is a menu callback for it (like imagecache or advagg). Input one domain per line: www.example.com'),
  );

  return system_settings_form($form);
}

/**
 * validate cdn_seo_admin_settings_form submissions.
 */
function cdn_seo_admin_settings_form_validate($form, &$form_state) {
  $op = $form_state['values']['op'];

  // Do not validate if resetting.
  if ($op == 'Reset to defaults') {
    return;
  }

  // Convert the list to an array.
  $blacklist = explode("\n", $form_state['values']['cdn_seo_blacklist']);
  $blacklist = array_filter(array_map('trim', $blacklist));

  // Simple check on the domain.
  foreach ($blacklist as $domain) {
    // Check for slashes.
    if (strpos($domain, '/') !== FALSE) {
      $bad = TRUE;
      form_set_error('cdn_seo_blacklist', t('Only provide the domain name. %domain is not formatted correctly; strip out the http:// or the trailing slash from the domain.', array('%domain' => $domain)));
    }

    // Make sure the domain is valid.
    $ip = gethostbyname($domain);
    $long = ip2long($ip);
    if ($long == -1 || $long === FALSE) {
      $bad = TRUE;
      form_set_error('cdn_seo_blacklist', t('The domain %domain is not valid.', array('%domain' => $domain)));
    }
  }

  // Set the value to the array.
  if (empty($bad) && $op == 'Save configuration') {
    $form_state['values']['cdn_seo_blacklist'] = array_combine($blacklist, $blacklist);
  }
}
