<?php

/**
 * @file
 * CDN SEO module
 */

/**
 * Default value to see if the CDN SEO module is enabled.
 */
define('CDN_SEO_ENABLED', TRUE);

/**
 * Defined value for the blacklist.
 */
define('CDN_SEO_BLACKLIST', 1);

/**
 * Default value to see what mode to use.
 */
define('CDN_SEO_HOST_MODE', CDN_SEO_BLACKLIST);

/**
 * Implementation of hook_menu().
 */
function cdn_seo_menu() {
  $file_path = drupal_get_path('module', 'cdn_seo');

  $items['admin/settings/cdn/seo'] = array(
    'title'             => 'SEO',
    'description'       => 'Configuration for CDN SEO.',
    'page callback'     => 'cdn_seo_admin_page',
    'type'              => MENU_LOCAL_TASK,
    'weight'            => -2,
    'access arguments'  => array('administer site configuration'),
    'file path'         => $file_path,
    'file'              => 'cdn_seo.admin.inc',
  );

  return $items;
}

/**
 * Implementation of hook_init().
 */
function cdn_seo_init() {
  // Exit early if this is disabled.
  if (!variable_get('cdn_seo_enabled', CDN_SEO_ENABLED)) {
    return;
  }

  // Get context.
  $host = empty($_SERVER['HTTP_HOST']) ? $_SERVER['SERVER_NAME'] : $_SERVER['HTTP_HOST'];
  $file_dir = file_directory_path();

  // Special handling for requests to the files dir.
  if (strpos($_GET['q'], $file_dir) === 0) {

    // If this path has a menu item then exit here and let the callback work it.
    $router_item = menu_get_item();
    if (!empty($router_item)) {
      return;
    }
  }

  // Get all CDN domains.
  $mode = variable_get('cdn_seo_host_mode', CDN_SEO_HOST_MODE);
  if ($mode == CDN_SEO_BLACKLIST) {
    $blacklisted_domains = variable_get('cdn_seo_blacklist', cdn_get_domains());
  }

  // If this host is blacklisted then fast 404.
  if (isset($blacklisted_domains[$host])) {
    cdn_seo_fast404('Blacklisted domain');
  }
}

/**
 * Send out a fast 404 and exit.
 *
 * @param $msg
 *   send this message in the header.
 */
function cdn_seo_fast404($msg = '') {
  global $base_path;
  if (!headers_sent()) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    header('X-CDN-SEO: ' . $msg);
  }
  print '<html>';
  print '<head><title>404 Not Found</title></head>';
  print '<body><h1>Not Found</h1>';
  print '<p>The requested URL was not found on this server.</p>';
  print '<p><a href="' . $base_path . '">Home</a></p>';
  print '</body></html>';
  exit();
}
